#flask container

FROM ubuntu:16.04

RUN apt-get update -y && \
    apt-get install -y python-pip python-dev

#add flask em requirements
COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip install -r requirements.txt

#add flask .py em pasta do container
COPY . /app

ENTRYPOINT [ "python" ]

CMD [ "app.py" ]